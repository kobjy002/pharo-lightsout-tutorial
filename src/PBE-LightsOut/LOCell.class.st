"
'LOCell' is one of the class used for the game LighsOut.
"
Class {
	#name : #LOCell,
	#superclass : #SimpleSwitchMorph,
	#instVars : [
		'mouseAction'
	],
	#category : #'PBE-LightsOut'
}

{ #category : #initialization }
LOCell >> initialize [
	super initialize.
	self label: ''.
	self borderWidth: 3.
	bounds := 0 @ 0 corner: 16 @ 16.
	offColor := Color palePeach.
	onColor := Color paleBlue darker.
	self useRoundedCorners.
	self turnOff
]

{ #category : #accessing }
LOCell >> mouseAction: aBlock [
	mouseAction := aBlock
]

{ #category : #'event handling' }
LOCell >> mouseUp: anEvent [
	self toggleState.
	mouseAction value
]
